from utils.gorest_handler import GoRESTHandler
from faker import Faker

gorest_handler = GoRESTHandler()


def test_create_user():
    """
    GoREST API: test user creation and update
    - creates a user with valid user data
    - verifies that the created user data matches the user data sent in the request
    - updates the user with new data and verifies that the user data was successfully updated
    - deletes the user and verifies that the user was successfully deleted
    """
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = gorest_handler.get_user(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]


    user_data_2 = {
        "name": Faker().name(),
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    user_id = body["id"]

    body = gorest_handler.update_user(user_id, user_data_2).json()
    assert body["email"] == user_data_2["email"]
    assert body["name"] == user_data_2["name"]
    response = gorest_handler.delete_user(user_id)
    assert response.status_code == 204
