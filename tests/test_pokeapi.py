from utils.pokeapi_handler import PokeAPIHandler


pokeapi_handler = PokeAPIHandler()


def test_default_list_of_pokemons():
    """
    Pokemon API: assert /v2/pokemon response:
    - is not empty
    - has status 200
    - has 1279 items
    - is under 1 second
    - is under 100 kilobytes
    """

    response = pokeapi_handler.get_list_of_pokemons()
    response_body = response.json()

    assert len(response_body["results"]) > 0
    assert response_body["results"]
    # assert "results" in response_body -> check only if list exists

    assert response.status_code == 200

    assert response_body["count"] == 1279

    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000

    response_size_kb = len(response.content) / 1000
    assert response_size_kb < 100


def test_pagination():
    """
    Pokemon API: assert /v2/pokemon response for paginated results:
    - response contains the correct number of results
    - the first result in the response corresponds to the correct offset
    - the last result in the response corresponds to the correct offset and limit
    """
    params = {
        "limit": 11,
        "offset": 21
    }

    response = pokeapi_handler.get_list_of_pokemons(params)
    body = response.json()

    assert len(body["results"]) == params["limit"]

    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + 1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + params['limit']}/"


def test_shapes():
    """
    PokeAPI: Assert /v2/pokemon-shape response:
    - response contains non-empty result set
    - response count matches the number of results
    - response for specific Pokemon shape is correct
    """
    body = pokeapi_handler.get_shapes_of_pokemons().json()
    assert body["count"] == len(body["results"])

    pokemon_shape = body["results"][2]["name"]
    body = pokeapi_handler.get_shapes_of_pokemons(pokemon_shape).json()
    assert body["id"] == 3






# def test_pokemon_api_pagination():
#     params = {
#         "limit": 10,
#         "offset": 20
#     }
#     response = requests.get("https://pokeapi.co/api/v2/pokemon", params=params)
#     body = response.json()
#     assert body["results"][0]["name"] == "spearow"
#     assert body["results"][-1]["name"] == "nidorina"
#     assert len(body["results"]) ==


# def test_default_list_of_Pokemon():
#     response = requests.get("https://pokeapi.co/api/v2/pokemon")
#     response_body = response.json()
#
#     assert len(response_body["results"]) > 0
#     assert response_body["results"]
#
# assert "results" in r
    # assert response.status_code == 200
    #
    # assert response_body["count"] == 1279
    #
    # response_time_ms = response.elapsed.microseconds // 1000
    # assert response_time_ms < 1000
    #
    # response_size_bytes = len(response.content)
    # assert response_size_bytes < 100*1000



# def test_pagination():
#     params = {
#         "limit": 10,
#         "offset": 20
#     }
#     response = requests.get("https://pokeapi.co/api/v2/pokemon", params=params)
#     body = response.json()
#     splitted = (body["results"][0]['url']).split("/")
#     assert int(splitted[-2]) == (params["offset"] + 1)
#     assert len(body["results"]) == 10









   # assert body["results"] != []
    # assert response.status_code == 200
    # assert len(body["results"]) == 1279

# def test_response_time_under_1s():
#     response = requests.get("https://pokeapi.co/api/v2/pokemon")
#     assert response.elapsed.total_seconds() < 1
#     print(response.elapsed.total_seconds())
