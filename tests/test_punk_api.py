import pytest
import requests


# 1
def test_default_beers():
    """
    Punk API: assert /v2/beers response:
    - first beer in response has ID 1
    - last beer in response has ID 25
    - response contains 25 beers
    """

    response = requests.get("https://api.punkapi.com/v2/beers")
    body = response.json()
    assert body[0]["id"] == 1
    assert body[-1]["id"] == 25
    assert len(body) == 25


# 2
def test_id_123():
    """Punk API: assert /v2/beers/123 response first beer in response has the requested ID 123 """
    response = requests.get("https://api.punkapi.com/v2/beers/123")
    body = response.json()
    assert body[0]["id"] == 123


# 3
def test_get_20_results_from_5_page():
    """
    Punk API: assert /v2/beers response for 20 beers from page 5:
    - response contains 20 beers
    - first beer in response has ID 81
    - last beer in response has ID 100
    """
    response = requests.get("https://api.punkapi.com/v2/beers?page=5&per_page=20")
    body = response.json()
    assert len(body) == 20
    assert body[0]["id"] == 81
    assert body[-1]["id"] == 100


# 4
def test_ids_11_to_20():
    """
    Punk API: assert /v2/beers response for beers with IDs 11 to 20:
    - response contains 10 beers
    - IDs of the beers in the response start at 11 and increase by 1
    """
    params = {
        "ids": "11|12|13|14|15|16|17|18|19|20"
    }
    response = requests.get("https://api.punkapi.com/v2/beers", params=params)
    body = response.json()
    assert len(body) == 10
    beer_id = 11
    for beer in body:
        assert beer["id"] == beer_id
        beer_id += 1


# 5
def test_abv_is_between_5_to_7():
    """
    Punk API: assert /v2/beers response for beers with ABV between 5% and 7%:
    - response contains beers with ABV values between 5% and 7%
    """
    response = requests.get("https://api.punkapi.com/v2/beers?abv_gt=4.9&abv_lt=7.1")
    body = response.json()
    for beer in body:
        assert beer["abv"] <= 7
        assert beer["abv"] >= 5


# # 6
def test_brewed_in_2010():
    """
    Punk API: assert /v2/beers response for beers brewed in 2010:
    - response contains only beers that were first brewed in 2010
    """
    response = requests.get("https://api.punkapi.com/v2/beers?brewed_before=01-2011&brewed_after=12-2009")
    body = response.json()
    for beer in body:
        first_brewed = beer["first_brewed"][-4:]
    assert first_brewed == str(2010)
